# dataway

## 介绍

### Dataway 是基于 DataQL 服务聚合能力，为应用提供的一个接口配置工具。使得使用者无需开发任何代码就配置一个满足需求的接口。整个接口配置、测试、冒烟、发布。一站式都通过 Dataway 提供的 UI 界面完成。UI 会以 Jar 包方式提供并集成到应用中并和应用共享同一个 http 端口，应用无需单独为 Dataway 开辟新的管理端口。

这种内嵌集成方式模式的优点是，可以使得大部分老项目都可以在无侵入的情况下直接应用 Dataway。进而改进老项目的迭代效率，大大减少企业项目研发成本。



- Dataway 工具化的提供 DataQL 配置能力。这种研发模式的变革使得，相当多的需求开发场景只需要配置即可完成交付。从而避免了从数据存取到前端接口之间的一系列开发任务，例如：Mapper、BO、VO、DO、DAO、Service、Controller 统统不在需要。

如上图所示 Dataway 在开发模式上提供了巨大的便捷。虽然工作流程中标识了由后端开发来配置 DataQL 接口，但这主要是出于考虑接口责任人。但在实际工作中根据实际情况需要，配置接口的人员可以是产品研发生命周期中任意一名角色。

## 主打场景

### 取数据

### 存数据

### 数据聚合

## 技术架构

###     Dataway 的架构中需要两张数据库表用来存放配置和发布的 DataQL 查询。

    除此之外 Dataway 的架构极其简单，它只是相当于给 DataQL 披了一张皮。通过这张皮以界面的方式交互式执行 DataQL 语句，并将其管理起来。

### ORM误区

-     ORM 最大的特点是具有 Mapping 过程，然后通过框架进行 CURD 操作。
        例如：Mybatis、Hibernate，其中有一些甚至做到了更高级的界面化例如： apijson，但其本质依然是 ORM。
    DataQL 有很大不同，虽然 DataQL 提供了非常出色的基于 SQL 数据存取能力。
        但是无论从任何角度去看它都没有 ORM 中最关键的 Mapping 过程。能够看到最接近的只有 DataQL 对数据的结果转换能力。
    造成 ORM 错觉的是由于 DataQL 充分利用 Udf 和 Fragment 奇妙的组合，提供了更便捷的数据库存储逻辑配置化。但从技术架构上来审视它并不是 ORM。

## 核心组件

### DataQL

- 介绍

	- DataQL（Data Query Language）DataQL 是一种查询语言。旨在通过提供直观、灵活的语法来描述客户端应用程序的数据需求和交互。

数据的存储根据其业务形式通常是较为简单的，并不适合直接在页面上进行展示。因此开发页面的前端工程师需要为此做大量的工作，这就是 DataQL 极力解决的问题。

例如：下面这个 DataQL 从 user 函数中查询 id 为 4 的用户相关信息并返回给应用。

- 特性

	- DataQL 有一些设计原则，这也使其成为有一定的特性。

    层次结构：多数产品都涉及数据的层次结构，为了保证结构的一致性 DataQL 结果也是分层的。
    数据为中心：前端工程是一个比较典型的场景，但是 DataQL 不局限于此（后端友好性）。
    弱类型定义：语言中不会要求声明任何形式的类型结构。
    简单逻辑：具备简单逻辑处理能力：表达式计算、对象取值、条件分支、lambda和函数。
    编译运行：查询的执行是基于编译结果的。
    混合语言：允许查询中混合任意的其它语言代码，典型的场景是查询中混合 SQL 查询语句。
    类 JS 语法：类JS语法设计，学习成本极低。

基于这些原则和特性，DataQL变为构建应用程序的强大而高效的环境。

目前 DataQL 提供了 Java 版的指令执行引擎，使用这个引擎您只需要依赖一个 Jar 包即可。任何一个 Web 应用或者 Spring Boot 的 jar 应用使用都变得非常容易。

- 语法

	-     a. 词法记号
    b. 类型系统
    c. 数值
    d. 语句
    e. 表达式
    f. 访问符
    g. 取值与赋值
    h. 结果转换
    i. 函数
    j. 混合其它语言

### 架构

## 扩展能力

###     AuthorizationChainSpi
    CompilerSpiListener
    FxSqlCheckChainSpi
    LoginPerformChainSpi（!!!）
    LoginTokenChainSpi（!!!）
    LookupConnectionListener
    LookupDataSourceListener
    PreExecuteChainSpi
    ResultProcessChainSpi
    SerializationChainSpi
    在页面中配置，在程序中调用

## 接口状态流转

###     Editor：编辑状态，接口不可访问。(api_status = 0)
    SmokeTest：冒烟测试是一个动作。只有冒烟测试通过之后才能进行发布操作，冒烟测试状态不会被持久保存。
    Published：接口已发布可以正常访问，并且无增量变化。(api_status = 1)
    Changes：接口已经发布可以正常访问，但是有后续变更尚未发布。此时访问接口是上一次发布的查询逻辑。(api_status = 2)
    Disable：无效接口，说明接口曾经发布成功过。此时将接口进行下线处理。在编辑器页面可以修改重新发布。(api_status = 3)
    Delete：接口已删除，被删除的接口会被真正的物理删除。interface_release 表中会保留历史数据。

## dataway

### 连建锋

### 2022-01

## 拓展

### 自定义udf

- 子主题 1

### 缓存机制

- 

Dataway 增加 PreExecuteChainSpi 扩展点，可以在 DataQL 执行之前进行干预。配合 ResultProcessChainSpi 可以实现缓存和权限。


### 与mp结合，利用主键生成策略，分页。

### 聚合查询引擎

