# 无限攀登

## Java 大法

### 自定义注解

#### 自定义日志注解

```java
package com.yq.code.system.logapi.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @author 连建锋
 * @date 2022/2/22
 * @QQ 2580040689@qq.com
 **/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SyStemLog {
    @AliasFor("description")
    String value() default "";

    /**
     * 描述
     * @return
     */
    @AliasFor("value")
    String description() default "";

    /**
     * 模块名
     * @return
     */
    String moduleName() default "";

    /**
     * 业务名
     * @return
     */
    String businessName() default "";
}
package com.yq.code.system.logapi.aspect;

import com.alibaba.fastjson.JSON;
import com.yq.code.authorization.api.AuthenticationHolder;
import com.yq.code.system.logapi.entity.SysOperateLog;
import com.yq.code.system.logapi.holder.RequestHolder;
import com.yq.code.system.logapi.service.SystemLogService;

import com.yq.code.utils.IPUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 连建锋
 * @date 2022/2/22
 * @QQ 2580040689@qq.com
 **/
@Aspect
@Slf4j
@Component
public class SystemLogAspect {
    @Autowired
    private SystemLogService systemLogService;

    private static ThreadLocal<ProceedingJoinPoint> proceedingJoinPointThreadLocal = new ThreadLocal<>();

    /**
     * 配置切入点
     */
    @Pointcut("@annotation(com.yq.code.system.logapi.annotation.SyStemLog)")
    public void logPointcut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }

    /**
     * 配置环绕通知,使用在方法logPointcut()上注册的切入点
     *
     * @param joinPoint join point for advice
     */
    @Around("logPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        proceedingJoinPointThreadLocal.set(joinPoint);
        HttpServletRequest request = RequestHolder.getRequest();
        // 开始时间
        long startTime = System.currentTimeMillis();
        SysOperateLog sysLog = new SysOperateLog();
        Object result = joinPoint.proceed();
        // 返回结果
        sysLog.setResponseDetail(JSON.toJSONString(result));
        String userId= AuthenticationHolder.get().getUser().getId();
        sysLog.setCreatorId(userId);
        sysLog.setUpdaterId(userId);
        // 方法类型
        sysLog.setMethodType(request.getMethod());
        // 请求路径
        sysLog.setRequestUrl(request.getRequestURL().toString());
        // 获取用户ip
        sysLog.setRequestIp(IPUtil.getIpAddr(request));
        systemLogService.asyncSaveLog(joinPoint, startTime, sysLog);
        return result;
    }

    @AfterThrowing(pointcut="logPointcut()",throwing = "exception")
    public void afterThrowing(Exception exception){
        ProceedingJoinPoint proceedingJoinPoint = proceedingJoinPointThreadLocal.get();
        HttpServletRequest request = RequestHolder.getRequest();
        SysOperateLog sysLog = new SysOperateLog();
        // 异常信息
        sysLog.setExceptionDetail(JSON.toJSONString(exception));
        String userId=AuthenticationHolder.get().getUser().getId();
        sysLog.setCreatorId(userId);
        sysLog.setUpdaterId(userId);
        // 方法类型
        sysLog.setMethodType(request.getMethod());
        // 请求路径
        sysLog.setRequestUrl(request.getRequestURL().toString());
        // 获取用户ip
        sysLog.setRequestIp(IPUtil.getIpAddr(request));
        systemLogService.asyncSaveLog(proceedingJoinPoint, null, sysLog);
    }
}
```

#### 自定义事件监听注解

```java
/**
 * @author 连建锋
 * @date 2022/3/3
 * @QQ 2580040689@qq.com
 **/
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ReinforceResult {
}
package com.yq.code.commons.model.aspect;

import com.yq.code.commons.model.event.ResultEvent;
import com.yq.code.commons.model.message.Result;
import com.yq.code.commons.utils.redis.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 连建锋
 * @date 2022/3/3
 * @DESC  切入事件增加，发起流程。
 * @QQ 2580040689@qq.com
 **/
@Aspect
@Slf4j
@Component
public class ReinforceResultAspect {

    RedisService redisService;

    private ApplicationEventPublisher applicationEventPublisher;

    public ReinforceResultAspect(RedisService redisService,ApplicationEventPublisher applicationEventPublisher){
        this.redisService = redisService;
        this.applicationEventPublisher = applicationEventPublisher;
    }


    /**
     * 配置切入点
     */
    @Pointcut("@annotation(com.yq.code.commons.model.annotation.ReinforceResult)")
    public void pointCut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }
    /**
     * 在上面定义的切面方法之前执行该方法
     * @param joinPoint jointPoint
     */
    @Before("pointCut()")
    public void doBefore(JoinPoint joinPoint) {
        log.info("====doBefore方法进入了====");

        // 获取签名
        Signature signature = joinPoint.getSignature();
        // 获取切入的包名
        String declaringTypeName = signature.getDeclaringTypeName();
        // 获取即将执行的方法名
        String funcName = signature.getName();
        log.info("即将执行方法为: {}，属于{}包", funcName, declaringTypeName);

        // 也可以用来记录一些信息，比如获取请求的 URL 和 IP
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 获取请求 URL
        String url = request.getRequestURL().toString();
        // 获取请求 IP
        String ip = request.getRemoteAddr();
        log.info("用户请求的url为：{}，ip地址为：{}", url, ip);
    }
    /**
     * 在上面定义的切面方法之后执行该方法
     * @param joinPoint jointPoint
     */
    @After("pointCut()")
    public void doAfter(JoinPoint  joinPoint) {

        log.info("==== doAfter 方法进入了====");
        Signature signature = joinPoint.getSignature();
        String method = signature.getName();
        log.info("方法{}已经执行完", method);
    }
    /**
     * 在上面定义的切面方法返回后执行该方法，可以捕获返回对象或者对返回对象进行增强
     * @param joinPoint joinPoint
     * @param result result
     */
    @AfterReturning(pointcut = "pointCut()", returning = "result")
    public void doAfterReturning(JoinPoint  joinPoint, Result result) {

        Signature signature = joinPoint.getSignature();
        String classMethod = signature.getName();
        log.info("方法{}执行完毕，返回参数为：{}", classMethod, result);
        // 实际项目中可以根据业务做具体的返回值增强
        log.info("对返回参数进行业务上的增强：{}", result + "增强版");
        //流程发布
        ResultEvent event=new ResultEvent(this);
        event.setResult(result);
        applicationEventPublisher.publishEvent(event);
    }
    /**
     * 在上面定义的切面方法执行抛异常时，执行该方法
     * @param joinPoint jointPoint
     * @param ex ex
     */
    @AfterThrowing(pointcut = "pointCut()", throwing = "ex")
    public void afterThrowing(JoinPoint joinPoint, Throwable ex) {
        Signature signature = joinPoint.getSignature();
        String method = signature.getName();
        // 处理异常的逻辑
        log.info("执行方法{}出错，异常为：{}", method, ex);
    }
}
```

## 设计模式

### 工厂模式&建造模式应用

#### 场景

河务局项目需要导出**不同事件**的**复杂表格**。

#### 简单工厂

```java
public Object exportProjectBaseInfo(String eventId,String projectName,String sbsj,String type) {
   return excelFactory.makeExcel(eventId,projectName,sbsj,type);
  
}
```

```java
 public class ExcelFactory {
    @Autowired
    DykeCheck dykeCheck;
    @Autowired
    WaterHighCheck waterHighCheck;
    @Autowired
    RiverCheck riverCheck;
    @Autowired
    DangerCheck dangerCheck;
    @Autowired
    WaterGateCheck waterGateCheck;
    @Autowired
    PatrolCheck patrolCheck;
    @Autowired
    FloodCheck floodCheck;
    @Autowired
    RiverRegulationCheck riverRegulationCheck;
    @Autowired
    BeachCollapseCheck beachCollapseCheck;
    @Autowired
    GateOvercurrentCheck gateOvercurrentCheck;
    @Autowired
    FloodplainCheck floodplainCheck;
    @Autowired
    FloodEmbankmentCheck floodEmbankmentCheck;
    @Autowired
    BeachObservationCheck beachObservationCheck;
    @Autowired
    DrinkingWaterCheck drinkingWaterCheck;
    @Autowired
    MaintenanceCheck maintenanceCheck;
    public  ExeclProduct makeExcel(String eventId,String projectName,String sbsj,String type) {   switch (type){
        case "堤防工程经常检查记录表": //1
            return dykeCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, "1"});
        case "堤防工程定期检查记录表": //2
            return dykeCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, "0"});
        case "水位观测记录表":       //3
           return  waterHighCheck.getExeclProduct(new String[]{eventId, projectName, sbsj,type});
        case "河势观测记录表":       //4
            return riverCheck.getExeclProduct(new String[]{eventId, projectName, sbsj,type});
        case "险情观测表":       //5
            return dangerCheck.getExeclProduct(new String[]{eventId, projectName, sbsj,type});
        case "水闸工程定期检查记录表": //6做完
            return waterGateCheck.getExeclProduct(new String[]{eventId, projectName, sbsj,type, "0"});
        case "水闸工程经常检查记录表": //6做完
            return waterGateCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type, "1"});
        case "黄河河道巡查登记表":    //8差流程数据
            return patrolCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        case "洪水期滩岸出水高度观测表": //9做完
            return floodCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        case "河道整治工程经常检查记录表": //10 做完
            return riverRegulationCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type, "1"});
        case "河道整治工程定期检查记录表": //11 做完
            return riverRegulationCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type, "0"});
        case "滩岸坍塌观测表":          //12 做完
            return beachCollapseCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        case "滩区口门过流观测表":       //13
            return gateOvercurrentCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        case "生产堤偎水观测表":  //15
            return floodplainCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        case "滩区漫滩及洪水偎堤观测表":  //16
            return floodEmbankmentCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        case "滩岸观测记录表":          //17
            return  beachObservationCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        case "逐日引水统计表":          //18
            return drinkingWaterCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        case "维修养护完成情况统计表":    //19
            return maintenanceCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
    }
    return null;

}}
```



## 服务层引用

```java
public ExeclProduct getExeclProduct(String... args) {
    String eventId = args[0];
    String projectName = args[1];
    String sbsj = args[2];
    String type = args[3];
    MaintenanceCheckHeader header=getHeader(eventId,projectName,sbsj);
    List<MaintenanceCheckBody> bodys=new ArrayList<>();
    bodys.addAll(getBodys(header,type));
    MaintenanceCheckFoot foot=getFoots();
    MaintenanceCheckBuilder builder=new MaintenanceCheckBuilder();
    ExcelDirector director=new ExcelDirector(builder);
    return director.construct(header,bodys,foot);
}
```

#### 抽象建造者

```java
public abstract class ExcelBuilder{
    protected ExeclProduct execlProduct= new ExeclProduct();
    public abstract void buildHeader(Object header );
    public abstract void buildBody(Object body);
    public abstract void buildPFoot(Object foot);
    //返回产品对象
    public ExeclProduct getResult() {
        return execlProduct;
    }
}
```

#### 目标产出

```java
public class ExeclProduct  {
    //execl 头部信息
    private Object head;
    //execl 主题信息 通常是list
    private Object body;
    //execl 主题信息
    private Object foot;
}
```



```java
public class ExcelFactory {
    @Autowired
    DykeCheck dykeCheck;
    @Autowired
    WaterHighCheck waterHighCheck;
    @Autowired
    RiverCheck riverCheck;
    @Autowired
    DangerCheck dangerCheck;
    @Autowired
    WaterGateCheck waterGateCheck;
    @Autowired
    PatrolCheck patrolCheck;
    @Autowired
    FloodCheck floodCheck;
    @Autowired
    RiverRegulationCheck riverRegulationCheck;
    @Autowired
    BeachCollapseCheck beachCollapseCheck;
    @Autowired
    GateOvercurrentCheck gateOvercurrentCheck;
    @Autowired
    FloodplainCheck floodplainCheck;
    @Autowired
    FloodEmbankmentCheck floodEmbankmentCheck;
    @Autowired
    BeachObservationCheck beachObservationCheck;
    @Autowired
    DrinkingWaterCheck drinkingWaterCheck;
    @Autowired
    MaintenanceCheck maintenanceCheck;
    public  ExeclProduct makeExcel(String eventId,String projectName,String sbsj,String type) {

        switch (type){
            case "堤防工程经常检查记录表": //1
                return dykeCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, "1"});
            case "堤防工程定期检查记录表": //2
                return dykeCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, "0"});
            case "水位观测记录表":       //3
               return  waterHighCheck.getExeclProduct(new String[]{eventId, projectName, sbsj,type});
            case "河势观测记录表":       //4
                return riverCheck.getExeclProduct(new String[]{eventId, projectName, sbsj,type});
            case "险情观测表":       //5
                return dangerCheck.getExeclProduct(new String[]{eventId, projectName, sbsj,type});
            case "水闸工程定期检查记录表": //6做完
                return waterGateCheck.getExeclProduct(new String[]{eventId, projectName, sbsj,type, "0"});
            case "水闸工程经常检查记录表": //6做完
                return waterGateCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type, "1"});
            case "黄河河道巡查登记表":    //8差流程数据
                return patrolCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
            case "洪水期滩岸出水高度观测表": //9做完
                return floodCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
            case "河道整治工程经常检查记录表": //10 做完
                return riverRegulationCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type, "1"});
            case "河道整治工程定期检查记录表": //11 做完
                return riverRegulationCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type, "0"});
            case "滩岸坍塌观测表":          //12 做完
                return beachCollapseCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
            case "滩区口门过流观测表":       //13
                return gateOvercurrentCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
            case "生产堤偎水观测表":  //15
                return floodplainCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
            case "滩区漫滩及洪水偎堤观测表":  //16
                return floodEmbankmentCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
            case "滩岸观测记录表":          //17
                return  beachObservationCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
            case "逐日引水统计表":          //18
                return drinkingWaterCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
            case "维修养护完成情况统计表":    //19
                return maintenanceCheck.getExeclProduct(new String[]{eventId, projectName, sbsj, type});
        }
        return null;

    }
}
```

#### 具体建造者

```java
public class PatrolCheckBuilder extends ExcelBuilder {

    @Override
    public void buildHeader(Object header ) {
      execlProduct.setHead(header);
    }

    @Override
    public void buildBody(Object body) {
        execlProduct.setBody(body);
    }

    @Override
    public void buildPFoot(Object foot) {
        execlProduct.setFoot(foot);
    }

}
```

## 

简单工厂和建造者模式同属于创建型设计模式。此代码用简单工厂来控制构建类型，建造来创建复杂对象。