# Flowable

## 什么是工作流？

### 工作流，是把业务之间的各个步骤以及规则进行抽象和概括性的描述。使用特定的语言为业务流程建模，让其运行在计算机上，并让计算机进行计算和推动。

- 工作流是复杂版本的状态机

### 那么工作流有什么不能解决的问题呢？

- 工作流是一个固定好的框架，大家就按照这个框架来执行流程就行了，但某些情况他本身没有流转顺序的要求，比如：小明每天需要做作业，做运动以及玩游戏，它们之间没有关联性无法建立流程，但可以根据每一项完成的状态决定今天的任务是否完结，这种情况我们需要使用CMMN来建模，它就是专门针对这种情况而设计的，但今天我们不讲这个，而是讲讲BPMN协议。

## BPMN2.0协议

对于业务建模，我们需要一种通用的语言来描绘，这样在沟通上和实现上会降低难度，就像中文、英文一样，BPMN2.0便是一种国际通用的建模语言，他能让自然人轻松阅读，更能被计算机所解析。



协议中元素的主要分类为，事件-任务-连线-网关。



一个流程必须包含一个事件（如：开始事件）和至少一个结束（事件）。其中网关的作用是流程流转逻辑的控制。任务则分很多类型，他们各司其职，所有节点均由连线联系起来。


### 

### 网关

网关的本质其实是计数器和出口逻辑的混合， 它跟其他节点没什么区别，只是他的推动逻辑需要使他的计数器为0，而计数器的总数为网关入口线段的数量，比如“组装”节点前面的并行网关，他的计数器就为4，而前面4个节点，每完成一个就会触发该网关计数器-1。 
当计数器为0的时候，网关会触发选择后续流转的逻辑。 


互斥网关的逻辑为：遍历所有出口连线，满足条件则流出并打断（也就是break掉）。 

并行网关的逻辑为：遍历所有出口连线，无条件为所有连线流出。 

包容性网关的逻辑为：遍历所有出口连线，满足条件的就流出。

- 互斥网关（Exclusive Gateway）

	- 又称排他网关，他有且仅有一个有效出口，可以理解为if......else if...... else if......else，就和我们平时写代码的一样。

- 并行网关（Parallel Gateway）

	- 他的所有出口都会被执行，可以理解为开多线程同时执行多个任务。

- 包容性网关（Inclusive Gateway）

	- 只要满足条件的出口都会执行，可以理解为 if(......) do, if (......) do, if (......) do，所有的条件判断都是同级别的。

### 任务

- 人工任务（User Task）

	- 它是使用得做多的一种任务类型，他自带有一些人工任务的变量，例如签收人（Assignee），签收人就代表该任务交由谁处理，我们也可以通过某个特定或一系列特定的签收人来查找待办任务。利用上面的行为解释便是，当到达User Task节点的时候，节点设置Assignee变量或等待设置Assignee变量，当任务被完成的时候，我们使用Trigger来要求流程引擎退出该任务，继续流转。

- 服务任务（Service Task）

	- 该任务会在到达的时候执行一段自动的逻辑并自动流转。从“到达自动执行一段逻辑”这里我们就可以发现，服务任务的想象空间就可以非常大，我们可以执行一段计算，执行发送邮件，执行RPC调用，而使用最广泛的则为HTTP调用，因为HTTP是使用最广泛的协议之一，它可以解决大部分第三方调用问题，在我们的使用中，HTTP服务任务也被我们单独剥离出来作为一个特殊任务节点。

- 接受任务（Receive Task）

	- 该任务的名字让人费解，但它又是最简单的一种任务，当该任务到达的时候，它不做任何逻辑，而是被动地等待Trigger，它的适用场景往往是一些不明确的阻塞，比如：一个复杂的计算需要等待很多条件，这些条件是需要人为来判断是否可以执行，而不是直接执行，这个时候，工作人员如果判断可以继续了，那么就Trigger一下使其流转。

- 调用活动（Call Activity）

	- 调用活动可以理解为函数调用，它会引用另外一个流程使之作为子流程运行，调用活动跟函数调用的功能一样，使流程模块化，增加复用的可能性。
上面大概介绍了一下常用的节点，下面的图就展示了一个以BPMN2.0为基础的流程模型，尽量覆盖到所介绍的所有节点。

### BPMN2.0为基础的流程模型

这里是一个生产汽车的流程，从“汽车设计”节点到“批准生产”节点是一个串行的任务，而审批的结果会遇到一个互斥网关，上面讲过，互斥网关只需要满足其中一个条件就会流转，而这里表达的意义就是审批是否通过。“载入图纸”是一个服务任务，它是自动执行的，之后会卡在“等待原材料”这个节点，因为这个节点是需要人为去判断（比如原材料涨价，原材料不足等因素），所以需要在一种自定义的条件下Trigger，而该图的条件应该为“原材料足够”，原材料足够之后，我们会开始并行生产汽车零件。



需要注意的是，并行网关在图中是成对出现的，他的作用是开始一系列并行任务和等待并行任务一起完成，拿一个Java中的东西举例子，就是CountDownLatch，fork-join模型也可以类比。


### 流对象（Flow Object）

- 事件（Events）

	- 启动事件

		- 空启动事件

			- 没有触发器，也就是手动触发

		- 定时启动事件

			- 流程需要指定时间启动或者指定时间间隔启动时使用。

		- 消息启动事件

			-  通过捕获消息的方式触发流程的启动（消息订阅）。一个流程中消息名是必须唯一，部署的多个流程中，也只能一个消息最多只能一个消息启动事件捕获，发版时一定会注销前版本的消息订阅。

		- 信号启动事件

			- 使用具名信号启动流程实例 ，信号可以由流程实例抛出，或者API方式触发。

		- 错误启动事件

			- 用于触发子流程，不能用于启动流程实例。总是中断的。

	- 结束事件

		- 空结束事件

			- 指引擎到此没有特别指定抛出的结果，结束当前分支外，不会多做任何事情。

		- 错误结束事件

			- 当流程执行到达错误结束事件（error end event）时，结束执行的当前分支，并抛出错误。这个错误可以由匹配的错误边界中间事件捕获。如果找不到匹配的错误边界事件，将会抛出异常。 错误结束事件会终止主流程的执行。

		- 终止结束事件

			- 到达终止结束事件时，当前的流程实例或子流程会被终止。概念上说，当执行到达终止结束事件时，会判断第一个范围 scope（流程或子流程）并终止它。请注意在BPMN 2.0中，子流程可以是嵌入式子流程，调用活动，事件子流程，或事务子流程。有一条通用规则：当存在多实例的调用过程或嵌入式子流程时，只会终止一个实例，其他的实例与流程实例不会受影响。可以添加一个可选属性terminateAll。当其为true时，无论该终止结束事件在流程定义中的位置，也无论它是否在子流程（甚至是嵌套子流程）中，都会终止（根）流程实例。


		- 取消结束事件

			- 取消结束事件（cancel end event）只能与BPMN事务子流程（BPMN transaction subprocess）一起使用。当到达取消结束事件时，会抛出取消事件，且必须由取消边界事件（cancel boundary event）捕获。取消边界事件将取消事务，并触发补偿


- 活动 （Activities）
- 网关 （Gateways）
- 流向/顺序流（Flow）

### 链接对象（Connecting Objects）

- 顺序流 （Sequence Flow）
- 消息流（Message Flow）
- 关联（Association）

### 泳道（Swimlanes）

- 池（Pool）
- 道（Lane）

### 附加工件（Arifacts/Artefacts）

- 数据对象（Data object）
- 组（group）
- 注释（Annotation）

## Flowable以及与其他工作流引擎的对比

Flowable项目提供了一组核心的开源业务流程引擎，这些引擎紧凑且高效。它们为开发人员、系统管理员和业务用户提供了一个工作流和业务流程管理（BPM）平台。它的核心是一个非常快速且经过测试的动态BPMN流程引擎。它基于Apache2.0开源协议，有稳定且经过认证的社区。

Flowable可以嵌入Java应用程序中运行，也可以作为服务器、集群运行，更可以提供云服务。

与大多数故事一样，Flowable也是因为其与Activiti对未来规划的路线不认同而开辟了一条自己的道路。目前主流的工作流开源框架就是Activiti/Camunda/Flowable，它们都有一个共同的祖先jbpm。先是有了jbpm4，随后出来了一个Activiti5，Activiti5经过一段时间的发展，核心人员出现分歧，又分出来了一个Camunda。activiti5发展了4年左右，紧接着就出现了Flowable。
相比于Activiti，Flowable的核心思想更像是在做一个多彩的工具，它在工作流的基础功能上，提供了很多其他的扩展，使用者可以随心所欲地把Flowable打造成自己想要的样子。例如：Camel节点，Mule节点。他不仅有bpmn引擎，还有cmmn（案例管理模型），dmn（决策模型），content（内容管理），form（表单管理），idm（用户鉴权）等等，但这也间接导致了Flowable的包结构非常繁多，上手非常困难。


### 

### Activiti

### Camunda

### Flowable

- 子主题 1

## Flowable的核心架构和一些改进

从架构图中可以看出，Flowable对于数据的处理是冷热分离的，热数据存在ACT_RU_系列表中，历史冷数据存在ACT_HI_系列表中，定义相关的存在ACT_RE_系列表中，而对于在途和定义相关的数据，有一层缓存，他缓存的具体实现比较复杂，这里不多赘述。
对于协议到运行态的转化，有专门一层Converter来实现，也就是说，如果你想自己定义一些协议外的东西，就需要关心这个部分。
最下面是节点行为层，这个很好理解，对于每种节点，都会有不同的实现，你也可以自定义实现。
Flowable在最新的版本中，对于历史归档和异步任务做了新的优化。


### Flowable开发了新的异步执行器（ASYNC EXECUTOR）

- 在工作流中，异步执行大概分为两类，timer和message，类似于定时事件就是timer，而异步的服务任务则为message，如上图所示，“Task A”附着的边界定时事件，在时间触发之后，会执行“Escalate”任务，而“Async Service Task”在“Task A”流转之后，会启用一个异步任务去调用其服务。
- 

	- 对于一种全是异步服务任务的极端情况，如上图所示，他常常出现于服务编排之类的场景之中，我们经常性的需要同时处理一系列的任务，这时候异步调度的作用就非常重要。

- 

	- 为了提高性能，Flowable也采用了冷热数据分离的思想，他把数据分为了4类，异步Job，定时器Timer，挂起任务，死信队列。通过测试发现，数据库是异步任务性能低下的主要瓶颈，特别是多实例竞争Job会存在潜在的问题。在分表的时候同时加上了一个全局锁，保证了同一个实例只能由一个实例去获取并锁定job（job表中有字段会被update，内容为抢占到的实例代号），这样反而能提升不少性能。为了保证各个实例不被饿死，还调整了一系列参数。

### 流程归档异步化

- Flowable提供了一个更加优化的冷热数据分离方案，在数据敏感性比较高的领域，我们一般会把引擎的历史记录级别调到最高（包括流程流转历史、变量变动历史、签收人变动历史等等），这些历史记录在以前是在同一个上下文中执行的，虽然在最开始设计的时候，在途数据和历史数据就冷热分离了，但从权衡在途和历史的重要性的角度来讲，历史其实不是最重要的，所以Flowable提供一系列的方法使历史记录这个行为异步化，与之对应的方法可以是jms，rabbitMQ或Spring Boot listener application。
这个改动可以提升在途流程效率20%-96%。

## 创建流程引擎

### 创建maven项目

<dependency>
            <groupId>org.flowable</groupId>
            <artifactId>flowable-spring-boot-starter</artifactId>
            <version>6.6.0</version>
        </dependency>

### 创建数据库表格

/**
 * @Author 三分恶
 * @Date 2020/5/2
 * @Description 创建表格
 */
public class HolidayRequest {
    public static void main(String[] args) {
        //1、创建ProcessEngineConfiguration实例,该实例可以配置与调整流程引擎的设置
        ProcessEngineConfiguration cfg=new StandaloneProcessEngineConfiguration()
                //2、通常采用xml配置文件创建ProcessEngineConfiguration，这里直接采用代码的方式
                //3、配置数据库相关参数
                .setJdbcUrl("jdbc:mysql://localhost:3306/flowable_demo?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2b8&nullCatalogMeansCurrent=true")
                .setJdbcUsername("root")
                .setJdbcPassword("root")
                .setJdbcDriver("com.mysql.jdbc.Driver")
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
        //4、初始化ProcessEngine流程引擎实例
        ProcessEngine processEngine=cfg.buildProcessEngine();
    }
}



### 34个表

34张表说明：

表分类 表名 表说明
 |  一般数据(2)  | ACT_GE_BYTEARRAY  | 通用的流程定义和流程资源 
 | ACT_GE_PROPERTY  | 系统相关属性
 | 流程历史记录(8)  | ACT_HI_ACTINST  | 历史的流程实例
 | ACT_HI_ATTACHMENT  | 历史的流程附件
 | ACT_HI_COMMENT  | 历史的说明性信息
 | ACT_HI_DETAIL  | 历史的流程运行中的细节信息
 | ACT_HI_IDENTITYLINK  | 历史的流程运行过程中用户关系 
 | ACT_HI_PROCINST  | 历史的流程实例
 | ACT_HI_TASKINST  | 历史的任务实例
 | ACT_HI_VARINST  | 历史的流程运行中的变量信息
 | 用户用户组表(9)  | ACT_ID_BYTEARRAY  | 二进制数据表
 | ACT_ID_GROUP  | 用户组信息表
 | ACT_ID_INFO  | 用户信息详情表
 | ACT_ID_MEMBERSHIP  | 人与组关系表
 | ACT_ID_PRIV  | 权限表 
 | ACT_ID_PRIV_MAPPING  | 用户或组权限关系表
 | ACT_ID_PROPERTY  | 属性表
 | ACT_ID_TOKEN  | 系统登录日志表
 | ACT_ID_USER  | 用户表
 | 流程定义表(3)  | ACT_RE_DEPLOYMENT  | 部署单元信息
 | ACT_RE_MODEL  | 模型信息
 | ACT_RE_PROCDEF  | 已部署的流程定义
 | 运行实例表(10)  | ACT_RU_DEADLETTER_JOB  | 正在运行的任务表
 | ACT_RU_EVENT_SUBSCR  | 运行时事件
 | ACT_RU_EXECUTION  | 运行时流程执行实例
 | ACT_RU_HISTORY_JOB  | 历史作业表
 | ACT_RU_IDENTITYLINK  | 运行时用户关系信息 
 | ACT_RU_JOB  | 运行时作业表
 | ACT_RU_SUSPENDED_JOB  | 暂停作业表
 | ACT_RU_TASK  | 运行时任务表
 | ACT_RU_TIMER_JOB  | 定时作业表
 | ACT_RU_VARIABLE  | 运行时变量表
 | 其他表(2)  | ACT_EVT_LOG  | 事件日志表
 | ACT_PROCDEF_INFO  | 流程定义信息

- flowable命名规则

	- ACT_RE_* ：’ RE ’表示repository（存储）。RepositoryService接口操作的表。带此前缀的表包含的是静态信息，如，流程定义，流程的资源（图片，规则等）。
	- ACT_RU_* ：’ RU ’表示runtime。这是运行时的表存储着流程变量，用户任务，变量，职责（job）等运行时的数据。flowable只存储实例执行期间的运行时数据，当流程实例结束时，将删除这些记录。这就保证了这些运行时的表小且快。
	- ACT_ID_* : ’ ID ’表示identity(组织机构)。这些表包含标识的信息，如用户，用户组，等等。
	- ACT_HI_* : ’ HI ’表示history。就是这些表包含着历史的相关数据，如结束的流程实例，变量，任务，等等。
	- ACT_GE_* : 普通数据，各种情况都使用的数据。

### 日志配置

- Flowable使用SLF4J作为内部日志框架。我们使用log4j作为SLF4J的实现。

### 关键对象

- Deployment：流程部署对象，部署一个流程时创建。
- ProcessDefinitions：流程定义，部署成功后自动创建。
- ProcessInstances：流程实例，启动流程时创建。
- Task：任务，在Activiti中的Task仅指有角色参与的任务，即定义中的UserTask。
- Execution：执行计划，流程实例和流程执行中的所有节点都是Execution，如UserTask、ServiceTask等。

### 服务接口

- ProcessEngine：流程引擎的抽象，通过它我们可以获得我们需要的一切服务。
- RepositoryService：Activiti中每一个不同版本的业务流程的定义都需要使用一些定义文件，部署文件和支持数据(例如BPMN2.0 XML文件，表单定义文件，流程定义图像文件等)，这些文件都存储在Activiti内建的Repository中。RepositoryService提供对 repository的存取服务。
- RuntimeService：在Activiti中，每当一个流程定义被启动一次之后，都会生成一个相应的流程对象实例。RuntimeService提供了启动流程、查询流程实例、设置获取流程实例变量等功能。此外它还提供了对流程部署，流程定义和流程实例的存取服务。
- TaskService: 在Activiti中业务流程定义中的每一个执行节点被称为一个Task，对流程中的数据存取，状态变更等操作均需要在Task中完成。TaskService提供了对用户Task 和Form相关的操作。它提供了运行时任务查询、领取、完成、删除以及变量设置等功能。
- IdentityService: Activiti中内置了用户以及组管理的功能，必须使用这些用户和组的信息才能获取到相应的Task。IdentityService提供了对Activiti 系统中的用户和组的管理功能。
- ManagementService: ManagementService提供了对Activiti流程引擎的管理和维护功能，这些功能不在工作流驱动的应用程序中使用，主要用于Activiti系统的日常维护。
- HistoryService: HistoryService用于获取正在运行或已经完成的流程实例的信息，与RuntimeService中获取的流程信息不同，历史信息包含已经持久化存储的永久信息，并已经被针对查询优化。

## 部署流程定义

要构建的流程是一个非常简单的请假流程。Flowable引擎需要流程定义为BPMN 2.0格式，这是一个业界广泛接受的XML标准。


在Flowable术语中，我们将其称为一个流程定义(process definition)。一个流程定义可以启动多个流程实例(process instance)。流程定义可以看做是重复执行流程的蓝图。 在这个例子中，流程定义定义了请假的各个步骤，而一个流程实例对应某个雇员提出的一个请假申请。


BPMN 2.0存储为XML，并包含可视化的部分：使用标准方式定义了每个步骤类型（人工任务，自动服务调用，等等）如何呈现，以及如何互相连接。这样BPMN 2.0标准使技术人员与业务人员能用双方都能理解的方式交流业务流程。


### 设计xml

### 加载xml，部署流程

-        //创建RepositoryService实例
        RepositoryService repositoryService=processEngine.getRepositoryService();
        //加载流程
        Deployment deployment=repositoryService.createDeployment()
                .addClasspathResource("xxxt.bpmn20.xml")
                .deploy();


### 查询验证已部署流程

-         //查询流程定义
        ProcessDefinition processDefinition=repositoryService.createProcessDefinitionQuery()
                .deploymentId(deployment.getId())
                .singleResult();
        System.out.println("Found process definition : "+processDefinition.getName());


## 启动流程实例

### 要启动流程实例，需要提供一些初始化流程变量。一般来说，可以通过呈现给用户的表单，或者在流程由其他系统自动触发时通过REST API，来获取这些变量。

### 接下来，我们使用RuntimeService启动一个流程实例。收集的数据作为一个java.util.Map实例传递，其中的键就是之后用于获取变量的标识符。这个流程实例使用key启动(还有其它方式)。

### 在流程实例启动后，会创建一个执行(execution)，并将其放在启动事件上。从这里开始，这个执行会沿着顺序流移动到经理审批的用户任务，并执行用户任务行为。这个行为将在数据库中创建一个任务，该任务可以之后使用查询找到。用户任务是一个等待状态(wait state)，引擎会停止执行，返回API调用处。

## Flowable中的事务

在Flowable中，数据库事务扮演了关键角色，用于保证数据一致性，并解决并发问题。当调用Flowable API时，默认情况下，所有操作都是同步的，并处于同一个事务下。这意味着，当方法调用返回时，会启动并提交一个事务。


流程启动后，会有一个数据库事务从流程实例启动时持续到下一个等待状态。在这个例子里，指的是第一个用户任务。当引擎到达这个用户任务时，状态会持久化至数据库，提交事务，并返回API调用处。


在Flowable中，当一个流程实例运行时，总会有一个数据库事务从前一个等待状态持续到下一个等待状态。数据持久化之后，可能在数据库中保存很长时间，甚至几年，直到某个API调用使流程实例继续执行。请注意当流程处在等待状态时，不会消耗任何计算或内存资源，直到下一次APi调用。


在这个例子中，当第一个用户任务完成时，会启动一个数据库事务，从用户任务开始，经过排他网关（自动逻辑），直到第二个用户任务。或通过另一条路径直接到达结束。


### 当调用Flowable API时，默认情况下，所有操作都是同步的，并处于同一个事务下。这意味着，当方法调用返回时，会启动并提交一个事务。

## 查询与完成任务

### 在更实际的应用中，会为雇员及经理提供用户界面，让他们可以登录并查看任务列表。其中可以看到作为流程变量存储的流程实例数据，并决定如何操作任务。

## 使用历史数据

### 选择使用Flowable这样的流程引擎的原因之一，是它可以自动存储所有流程实例的审计数据或历史数据。这些数据可以用于创建报告，深入展现组织运行的情况，瓶颈在哪里，等等。

从ProcessEngine获取HistoryService，并创建历史活动(historical activities)的查询。

## 信息

### 连建锋

### 2022-05

### Flowable工作流

